package com.bignerdranch.android.musicshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    int a = 0;
    Spinner spinner;
    ArrayList spinnerArrayList;
    ArrayAdapter spinnerAdapter;
    HashMap goodsMap;
    String goodsName;
    double price;
    EditText userNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = findViewById(R.id.spinner);
        spinnerArrayList = new ArrayList();
        spinner.setOnItemSelectedListener(this);
        spinnerArrayList.add("guitar");
        spinnerArrayList.add("drums");
        spinnerArrayList.add("cake");
        spinnerArrayList.add("keyboard");
        spinnerArrayList.add("cheese");

        spinnerAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerArrayList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);

        goodsMap = new HashMap();
        goodsMap.put("guitar", 500.0);
        goodsMap.put("drums", 1500.0);
        goodsMap.put("cake", 300.0);
        goodsMap.put("keyboard", 800.0);
        goodsMap.put("cheese", 10000.0);

        userNameEditText = findViewById(R.id.editTextTextPersonName);
    }

    public void OnePlus(View view) {
        a = a + 1;
        TextView helloTextView = findViewById(R.id.numberTextView);
        helloTextView.setText("" + a);
        TextView priceTextView = findViewById(R.id.priceTextView);
        priceTextView.setText("" + a * price);


    }

    public void minuses(View view) {
        if (a > 0 && a != 0){
        a = a -1;}
        else { a = 0;}
        TextView helloTextView = findViewById(R.id.numberTextView);
        helloTextView.setText("" + a);
        TextView priceTextView = findViewById(R.id.priceTextView);
        priceTextView.setText("" + a * price);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        goodsName = spinner.getSelectedItem().toString();
        price = (double)goodsMap.get(goodsName);
        TextView priceTextView = findViewById(R.id.priceTextView);
        priceTextView.setText("" + a * price);

        ImageView goodsImageView = findViewById(R.id.goodsImageView);
        if (goodsName.equals("guitar")){
            goodsImageView.setImageResource(R.drawable.guitarone);
        } else if (goodsName.equals("drums")){
            goodsImageView.setImageResource(R.drawable.drums);
        } else if (goodsName.equals("keyboard")){
            goodsImageView.setImageResource(R.drawable.keyboard);
        } else if (goodsName.equals("cake")){
            goodsImageView.setImageResource(R.drawable.cake);
        } else if (goodsName.equals("cheese")){
            goodsImageView.setImageResource(R.drawable.cheese);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void addToCard(View view) {
        Order order = new Order();
        order.userName = userNameEditText.getText().toString();
        Log.d("UserName", order.userName);
        order.goodsName = goodsName;
        order.quantity = a;
        order.orderPrice = a * price;
        Intent orderIntent = new Intent(MainActivity.this,OrderActivity.class);
        orderIntent.putExtra("userNameForIntent", order.userName);
        orderIntent.putExtra("goodsName", order.goodsName);
        orderIntent.putExtra("quantity", order.quantity);
        orderIntent.putExtra("orderPrice", order.orderPrice);
        startActivity(orderIntent);

    }
}